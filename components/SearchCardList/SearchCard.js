import React from 'react';
import { View, StyleSheet, TouchableOpacity } from 'react-native';
import { Avatar } from 'react-native-elements';

const SearchCard = (props) => {
    return (
        <TouchableOpacity style={styles.itemCard} onPress={() => console.log("pressed me", props)}>
            <Avatar size="large" activeOpacity={0.7} rounded icon={{ name: 'user', type: 'font-awesome', color: 'grey' }} />
            <View style={styles.contentCard}>
                {props.children}
            </View>
        </TouchableOpacity>
    )
}

export default SearchCard;

const styles = StyleSheet.create({
    profileIcon: {
        width: 100,
    },
    itemCard: {
        backgroundColor: "white",
        height: 100,
        flexDirection: "row",
        marginVertical: 8,
        borderRadius: 6,
        elevation: 3,
        shadowOffset: { width: 1, height: 1 },
        shadowColor: '#333',
        shadowOpacity: 0.3,
        // padding: 10,
        flex: 1
        // border: "5 solid black"
    },
    fontSize: {
        fontSize: 25,
    },
    contentCard: {
        flexDirection: "column",
        padding: 5,
        justifyContent: 'center'
    }
})