import React from 'react';
import { FlatList, Text } from 'react-native';
import SearchCard from './SearchCard';

// bring data in from the page
export default function SearchCardList({ DATA }) {
    return (
        <FlatList
            data={DATA}
            keyExtractor={item => item.id}
            renderItem={({ item }) =>
                <SearchCard>
                    {Object.entries(item).map(([key, value]) =>
                        key !== 'id' && <Text key={key}>{value}</Text>
                    )}
                </SearchCard>}
        />
    )
}