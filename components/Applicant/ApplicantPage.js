import React, { useState, useEffect } from 'react';
import { View, ActivityIndicator } from 'react-native';
import { SearchBar, Header } from 'react-native-elements';
import SearchCardList from '../SearchCardList/SearchCardList';
import filter from 'lodash.filter';

export default function ApplicantPage({ navigation }) {

    const [isLoading, setIsLoading] = useState(false);
    const [search, setSearch] = useState("");
    const [dataSet, setDataSet] = useState([]);
    const [error, setError] = useState(null);

    const DATA = [
        {
            id: '123',
            name: 'Tin',
            lastName: 'Dang',
            phoneNumber: '888-888-8888'

        },
        {
            id: '12',
            name: 'Souraj'
        },
        {
            id: '1265',
            name: 'Jack'
        },
        {
            id: '12435',
            name: 'Solomon'
        },
        {
            id: '1362',
            name: 'Elizabeth'
        },
        {
            id: '1742',
            name: 'Sam'
        },
        {
            id: '1452',
            name: 'Henry'
        },
        {
            id: '145234',
            name: 'Jackson'
        },
    ]

    const API_ENDPOINT = 'https://randomuser.me/api/?seed=1&page=1&results=20`'
    useEffect(() => {
        setIsLoading(true);

        fetch(API_ENDPOINT)
            .then(response => response.json())
            .then(results => {
                setDataSet(DATA)
                setIsLoading(false);
            })
            .catch(err => {
                setIsLoading(false);
                setError(err);
            });

    }, []);

    const handleSearch = text => {
        setSearch(text)
        if(text.length > 2){
            // const query = text.toLowerCase();
            const filteredData = filter(dataSet, user => {
                return contains(user, text)
            })
            setDataSet(filteredData)
        }
    }

    const contains = ({name}, query) => {
        if (name.includes(query)){
            return true;
        }
        return false;
    }

    if (isLoading) {
        return (
            <View style={{
                position: 'absolute',
                left: 0,
                right: 0,
                top: 0,
                bottom: 0,
                // opacity: 0.5,
                justifyContent: 'center',
                alignItems: 'center'
            }
    }>
                <ActivityIndicator size="large" color="#5500dc" />
            </View>
        );
    }

    if (error) {
        return (
            <View>
                <Text style={{ fontSize: 18 }}>
                    Error fetching data... Check your network connection!
                </Text>
            </View>
        );
    }


    return (
        <View style={{flex: 1}}>
            <Header
                // style={{width: "100%"}} 
                leftComponent={{ icon: 'menu', color: '#fff', onPress: () => navigation.toggleDrawer() }}
                centerComponent={{ text: 'Applicant Search', style: { color: '#fff'}}}
            />
            {/* <Button
                onPress={() => navigation.navigate('Home')}
                title="Return to Home"
            /> */}
            <SearchBar
                containerStyle={{borderWidth: 1, backgroundColor: "white"}}
                inputContainerStyle={{backgroundColor: "white", borderWidth: 1}}
                platform="default"
                placeholder="Type here"
                searchIcon={false}
                onChangeText={e => handleSearch(e)}
                value={search}
                onClear={() => setDataSet(DATA)}
            //showLoading --- for api calls
            />
            <SearchCardList
                DATA={dataSet}
            />
        </View>
    )
}