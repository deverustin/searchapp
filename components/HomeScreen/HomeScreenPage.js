import React from 'react';
import { View, ImageBackground } from 'react-native';
import { Header, Button } from 'react-native-elements';

export default function HomeScreenPage({ navigation }) {
    const urlImage = { uri: "https://vero75.stage.deverus.com/mvp/tools/file_viewer.cfm?bgid=11410_4A9220CD-E51F-49E6-8E1F-AFCDEC2B9475" }
    return (
        <View style={{ flex: 1 }}>
            <ImageBackground source={urlImage} style={{ flex: 1, resizeMode: "cover", justifyContent: "center" }}>
                <Header
                    leftComponent={{ icon: 'menu', color: '#fff', onPress: () => navigation.toggleDrawer() }}
                    centerComponent={{ text: 'Search App', style: { color: '#fff' } }}
                />
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Button
                        onPress={() => navigation.navigate('Applicant')}
                        title="Go to Applicant Search"
                        style={{marginVertical: 5}}
                    />
                    <Button
                        onPress={() => navigation.navigate('CRA')}
                        title="Go to CRA Search"
                        style={{marginVertical: 5}}
                    />
                </View>
            </ImageBackground>
        </View>
    )
}