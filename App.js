import React from 'react';
import { createDrawerNavigator } from '@react-navigation/drawer';
import { NavigationContainer } from '@react-navigation/native';
import HomeScreen from './components/HomeScreen/HomeScreenPage';
import CRA from './components/CRA/craPage';
import Applicant from './components/Applicant/ApplicantPage';

const Drawer = createDrawerNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen name="Home" component={HomeScreen} />
        <Drawer.Screen name="Applicant" component={Applicant} />
        <Drawer.Screen name="CRA" component={CRA} />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}